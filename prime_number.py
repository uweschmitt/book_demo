def is_prime(n):

    if n == 2:
        return True
    if n % 2 == 0:
        return False

    divisor = 3
    while divisor * divisor <= n:
        if n % divisor == 0:
            return False
        divisor += 2
    return True

def primes_up_to(n):
    return [i for i in range(2, n + 1) if is_prime(i)]
